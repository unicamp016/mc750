#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <MQTTClient.h>


const char client_id[] = "microcontroller";     //arbitrary identification
const char client_key[] = "cc63f20c";                   //token KEY from shiftr.io
const char client_secret[] = "366348a04f4c2244";                //token SECRET from shiftr.io

const char ssid[] = "RafaelNet";     //name of the used Wi-Fi network
const char pass[] = "jghg0456";     //password of the Wi-Fi network

WiFiClient net;
MQTTClient client;
const int QoS = 1;

const int face_pin = D1;
const int red_obj_pin = D5;
const int green_obj_pin = D6;
const int blue_obj_pin = D7;
const int yellow_obj_pin = D8;

int red = 0;
int green = 0;
int blue = 0;
int yellow = 0;
int face = 0;

void messageReceived(String &topic, String &payload)
{
    Serial.println("New message: " + topic + " - " + payload);

    if (topic == "/red_obj") {
      red = payload.toInt();
    }
    if (topic == "/blue_obj") {
      blue = payload.toInt();
    }
    if (topic == "/green_obj") {
      green = payload.toInt();
    }
    if (topic == "/yellow_obj") {
      yellow = payload.toInt();
    }
    if (topic == "/face") {
      face = payload.toInt();
    }

}

void connectWIFI()
{
    Serial.print("Connecting Wi-Fi: ");
    WiFi.begin(ssid, pass);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println(" Wi-Fi connected!");
}

void connectMQTT()
{
    Serial.print("Connecting MQTT: ");
    while (!client.connect(client_id, client_key, client_secret))
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println(" MQTT connected!");
}

void setup() {
    Serial.begin(115200);

    pinMode(red_obj_pin, OUTPUT);
    pinMode(blue_obj_pin, OUTPUT);
    pinMode(green_obj_pin, OUTPUT);
    pinMode(yellow_obj_pin, OUTPUT);
    pinMode(face_pin, OUTPUT);

    connectWIFI();
    client.begin("broker.shiftr.io", net);
    client.onMessage(messageReceived);
    connectMQTT();

    client.subscribe("/red_obj");
    client.subscribe("/blue_obj");
    client.subscribe("/green_obj");
    client.subscribe("/yellow_obj");
    client.subscribe("/face");

}



void loop() {
    // put your main code here, to run repeatedly:
    if (WiFi.status() != WL_CONNECTED)
    {
        connectWIFI();
    }
    client.loop();
    delay(10);  // fixes some issues with WiFi stability
    if (!client.connected())
    {
        connectMQTT();
    }

    analogWrite(red_obj_pin, red);
    analogWrite(green_obj_pin, green);
    analogWrite(blue_obj_pin, blue);
    analogWrite(yellow_obj_pin, yellow);
    analogWrite(face_pin, face);

}
