import cv2
from capture import capture, setRes
from mqtt import publish
from face import wait_for_face

PARAMS = {
    'camera_index': 1,
    'y_thr': 0.5,
    'x_thr': 0.5,
    'correct_colors': {
        'red': lambda p: p[0] < PARAMS['x_thr'] and p[1] < PARAMS['y_thr'],
        'green': lambda p: p[0] > PARAMS['x_thr'] and p[1] < PARAMS['y_thr'],
        'blue': lambda p: p[0] < PARAMS['x_thr'] and p[1] > PARAMS['y_thr'],
        'yellow': lambda p: p[0] > PARAMS['x_thr'] and p[1] > PARAMS['y_thr'],
    }
}

def main():
    print("Initializing camera object...")
    cap = cv2.VideoCapture(PARAMS["camera_index"])
    setRes(cap, 640, 480)
    print("Done!")

    states = {color: 0 for color in PARAMS['correct_colors']}

    publish('/face', 0)
    for color in states:
        publish('/%s_obj' % color, 0)

    try:
        has_face, __ = wait_for_face()

        publish('/face', 1)
        while True:
            colors_loc = capture(cap, img_window=True, show_masks=True)
            for color in PARAMS['correct_colors']:
                if color in colors_loc and PARAMS['correct_colors'][color](colors_loc[color]):
                    if states[color] == 0:
                        states[color] = 1
                        publish("/%s_obj" % color, 1)
                else:
                    if states[color] == 1:
                        states[color] = 0
                        publish("/%s_obj" % color, 0)
    except Exception as e:
        print(e)
    finally:
        print("Releasing camera...")
        cv2.destroyAllWindows()
        cap.release()

if __name__ == '__main__':
    main()
