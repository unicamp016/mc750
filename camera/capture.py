import cv2
import numpy as np

PARAMS = {
    "camera_index" : 0,
    "height" : -1,
    "width" : -1,
    "colors" : {
        "red": {"lower_1": [0, 200, 120], "lower_2": [235, 180, 110],
                "upper_1": [15, 255, 200], "upper_2": [255, 255, 255],
                "found_centroid"  : False,
                "centroid_color"  : [0, 0, 255],
                "centroid_coords" : (0, 0),
                "mask":  None
                },
        "blue": {"lower": [90, 150, 100],
                "upper": [120, 255, 200],
                "found_centroid"  : False,
                "centroid_color"  : [255, 0, 0],
                "centroid_coords" : (0, 0),
                "mask":  None
                },
        "green": {"lower": [60, 100, 50],
                "upper": [90, 180, 180],
                "found_centroid"  : False,
                "centroid_color"  : [0, 255, 0],
                "centroid_coords" : (0, 0),
                "mask":  None
                },
        "yellow": {"lower": [14, 154, 160],
                "upper": [34, 180, 255],
                "found_centroid"  : False,
                "centroid_color"  : [0, 120, 120],
                "centroid_coords" : (0, 0),
                "mask":  None
                }

    }
}

def getMaxCentroid(contours):
    # Find areas for all contours
    area_list = [cv2.contourArea(cnt) for cnt in contours]

    # Retrieve contour for largest area.
    max_area = np.max(area_list)
    max_contour = contours[area_list.index(max_area)]

    # Find centroid of max contour
    M = cv2.moments(max_contour)
    cx = M['m10']/M['m00']
    cy = M['m01']/M['m00']

    return int(cx), int(cy)

def getContours(img):
    contours = []

    _, cnts, _ = cv2.findContours(img, mode=1, method=2)
    for cnt in cnts:
        rect = cv2.boundingRect(cnt)
        if rect[0] > 0 and rect[1] > 0 and rect[1] < PARAMS["width"] and rect[3] < PARAMS["height"]:
            contours.append(cnt)
    return contours

def getMorph(morph):
    for i in range(1,4):
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2*i+1, 2*i+1))
        morph = cv2.morphologyEx(morph, cv2.MORPH_CLOSE, kernel)
        morph = cv2.morphologyEx(morph, cv2.MORPH_OPEN, kernel)
    return morph

def getMask(img, lower, upper, color_id):
    lower = np.array(PARAMS["colors"][color_id]["lower"])
    upper = np.array(PARAMS["colors"][color_id]["upper"])

    return cv2.inRange(img, lower, upper)

def drawCentroid(img, coords, k_dim, color):
    cx, cy = coords
    k = int(k_dim/2)
    for i in range(cy-k, cy+k+1):
        for j in range(cx-k, cx+k+1):
            img[i%PARAMS["height"]][j%PARAMS["width"]] = color
    return img

def filterIcons(img):
    w = PARAMS["width"]
    h = PARAMS["height"]

    icon_dim = 16
    mask = np.ones(img.shape, dtype=np.uint8)
    mask = mask*255

    x_origin = w/2-w/icon_dim
    y_origin = 0
    for i in range(4):
        x, y = (i//2, i%2)
        x_quad_origin = int(x_origin + x*w/2)
        y_quad_origin = int(y_origin + y*h/2)

        for m in range(int(w/icon_dim)):
            for n in range(int(h/icon_dim)):
                mask[y_quad_origin+n][x_quad_origin+m] = 0

    return cv2.bitwise_and(img, img, mask=mask)


def setRes(cap, x,y):
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, int(x))
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, int(y))
    return str(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),str(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

def capture(cap, img_window=False, show_masks=False):
    # Grab VideoCapture object
    #cap = cv2.VideoCapture(PARAMS["camera_index"])

    # Set params
    PARAMS["width"]  = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    PARAMS["height"] = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    normalize = lambda x, y: (x / PARAMS['width'], y / PARAMS['height'])
    denormalize = lambda p: (int(p[0] * PARAMS['width']), int(p[1] * PARAMS['height']))

    color_ids = [id for id in PARAMS["colors"].keys()]

    ret, frame = cap.read()

    # Transform frame to HSV colorspace
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Create list with all color masks
    mask_list = []
    for color_id in color_ids:
        args = PARAMS["colors"][color_id]
        if "red" in color_id:
            mask_1 = cv2.inRange(hsv, np.array(args["lower_1"]), np.array(args["upper_1"]))
            mask_2 = cv2.inRange(hsv, np.array(args["lower_2"]), np.array(args["upper_2"]))
            mask_list.append((color_id, cv2.bitwise_or(mask_1, mask_2)))

        else:
            mask_list.append((color_id, cv2.inRange(hsv, np.array(args["lower"]), np.array(args["upper"]))))


    centroids = {}
    for color_id, mask in mask_list:
        #Get morphological image transformation
        morph = getMorph(mask)

        #Filter out icon areas
        morph = filterIcons(morph)

        #Get object contours
        contours = getContours(morph)

        #Set default params
        PARAMS["colors"][color_id]["found_centroid"] = False
        PARAMS["colors"][color_id]["mask"] = None

        if show_masks:
            cv2.imshow(color_id, mask)
            cv2.waitKey(1)

        #If we find a contour, we're interested in the object with max area.
        if contours.__len__() != 0:
            cx, cy = getMaxCentroid(contours)

            centroids[color_id] = normalize(cx, cy)
            PARAMS["colors"][color_id]["found_centroid"] = True
            PARAMS["colors"][color_id]["mask"] = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
            PARAMS["colors"][color_id]["centroid_coord"] = (cx, cy)

    #Draw centroids
    if img_window:
        for color_id in centroids:
            cv2.circle(frame, denormalize(centroids[color_id]), 5, PARAMS['colors'][color_id]['centroid_color'], -1)
        cv2.imshow("final", frame)
        cv2.waitKey(1)

    return centroids
