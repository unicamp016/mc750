import cv2
import time
import urllib
import requests
import os
import glob
import numpy as np

CONFIG = {
    'camera': 0,
    'camera_res' : (1920, 1080),
    'face_wait_time': 5, # Amount of time face should be within sight.
    'face_timeout': -1, # -1: no timeout. Timeout for first face to appear
    'tmp_dir': './tmp',
    'init_camera_delay': 3, #Time to ignore initial input
    'face_api_endpoint': 'https://westcentralus.api.cognitive.microsoft.com/face/v1.0/',
    'face_api_key': '9bf284a934614afea4bf4f874df89760',
    'verbose': True
}

def setRes(cap, x,y):
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, int(x))
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, int(y))
    return str(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),str(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

def wait_for_face():
    # cap = cv2.VideoCapture(CONFIG['camera'])

    #Create tmp image dir
    if not os.path.isdir(CONFIG['tmp_dir']):
        os.mkdir(CONFIG['tmp_dir'])

    #Clean tmp image dir
    tmp_img_fps = glob.glob(os.path.join(CONFIG['tmp_dir'], "*"))
    for fp in tmp_img_fps:
        os.remove(fp)

    # Face API
    headers = {
        'Content-Type': 'application/octet-stream',
        'Ocp-Apim-Subscription-Key': CONFIG['face_api_key'],
    }
    params = urllib.parse.urlencode({
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses',
    })

    url = '%sdetect?%s' % (CONFIG['face_api_endpoint'], params)

    req_time = []
    frame_count = 0
    face_t = -1 # -1: no face appeared
    has_face = False
    init_t = time.time()
    if CONFIG['verbose']:
        print("Started looking for faces.")
    while face_t == -1 or time.time() - face_t <= CONFIG['face_wait_time']:
        if CONFIG['verbose']:
            if face_t != -1:
                print("Found someone {:.2f}s ago.".format(time.time()-face_t))

        cap = cv2.VideoCapture(CONFIG['camera'])
        setRes(cap, 1920, 1080)
        ret, frame = cap.read()
        cap.release()

        frame_count += 1

        tmp_file = os.path.join(CONFIG['tmp_dir'], "tmp_"+str(frame_count)+".jpg")
        cv2.imwrite(tmp_file, frame)

        img = open(tmp_file, 'rb')

        req_init_time = time.time()
        response = requests.post(url, data=img, headers=headers)
        req_time.append(time.time()-req_init_time)

        res = response.json()


        if len(res) > 0: # has found a face
            has_face = True
            if face_t == -1:
                if CONFIG['verbose']:
                    print("First face found!")
                face_t = time.time()
        else:
            if CONFIG['verbose']:
                if has_face:
                    print("Lost face :(")
                else:
                    print("Looking for face...")
            face_t = -1
            has_face = False

        # check timeout reached
        if CONFIG['face_timeout'] != -1 and time.time() - init_t > CONFIG['face_timeout']:
            if CONFIG['verbose']:
                print("Haven't found a face in {:.2f}s... now exiting".format(CONFIG['face_timeout']))
            break

    if CONFIG['verbose']:
        print("Average request took {:.3f}s".format(np.mean(req_time)))

    cap.release()
    return has_face, face_t

def test():
    has_face, face_t = wait_for_face()
    if CONFIG['verbose']:
        if has_face:
            print("Detected face for {:.2f}s... exiting sucessfully.".format(time.time()-face_t))

if __name__ == '__main__':
    test()
