import numpy as np

def filterIcons(img):
    # w = PARAMS["width"]
    # h = PARAMS["height"]
    w = img.shape[0]
    h = img.shape[1]

    print(img.shape)

    mask = np.ones(img.shape, dtype=np.float32)

    x_origin = w/2-w/8
    y_origin = 0
    for i in range(4):
        x, y = (i//2, i%2)
        print(x, y  )
        x_quad_origin = int(x_origin + x*w/2)
        y_quad_origin = int(y_origin + y*h/2)
        print(x_quad_origin, y_quad_origin)

        for m in range(int(w/8)):
            for n in range(int(h/8)):
                # print(m, n)
                mask[x_quad_origin+m][y_quad_origin+n] = 0

    print(mask.T)

if __name__ == "__main__":
    filterIcons(np.zeros((16, 16)))
